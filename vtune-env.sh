#!/bin/sh

# Modify this according to your vtune environment

export VTUNE_PATH='/path/to/vtune/'

export VTUNE_COMMAND="$VTUNE_PATH/vtune"

# Collection type
export VTUNE_TYPE=hotspots

# Any extra opts to use
export VTUNE_OPTS=
