-------------- Template Game --------------
This folder contains everything you need to 
create a basic game on Wavelength. It also 
contains useful git config files, and some 
other things. Make sure to read the source 
code of game/template before starting!
