#version 330

layout(location=0) in vec4 vPosition;
layout(location=1) in vec4 vTexCoord;
layout(location=2) in vec3 vNormal;

void main()
{
    gl_FragColor = vec4(1.0, 0.0, 0.0, 0.2);
}