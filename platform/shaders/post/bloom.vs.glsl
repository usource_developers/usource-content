#version 330
 
layout(location=0) in vec4 vPosition;
layout(location=1) in vec4 vTexCoord;
layout(location=2) in vec3 vNormal;

uniform mat4 mTransform;

void main()
{
    gl_Position = vPosition * mTransform;
}