#!/bin/bash

FILES=$(find . -iname "*.png")
for i in $FILES; do
        convert $i $(echo $i | sed 's/.png/.tga/g')
done
