#!/bin/bash

# figure out the absolute path to the script being run a bit
# non-obvious, the ${0%/*} pulls the path out of $0, cd's into the
# specified directory, then uses $PWD to figure out where that
# directory lives - and all this in a subshell, so we don't affect
# $PWD

GAMEROOT=$(cd "${0%/*}" && echo $PWD)
set -e

LAUNCHER_NAME="bin/linux64/hl"

# and launch the game
cd "$GAMEROOT"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$(pwd)/bin/linux64"
if [[ ! -z $GAME_DEBUGGER ]]; then 
	case "$GAME_DEBUGGER" in
		*gdb)
			"$GAME_DEBUGGER" $DEBUGGER_ARGS --args ./$LAUNCHER_NAME -game hl -basedir "$GAMEROOT" $@
			;;
		*rr)
			"$GAME_DEBUGGER" $DEBUGGER_ARGS record ./$LAUNCHER_NAME -game hl -basedir "$GAMEROOT" $@
			;;
		*valgrind)
			"$GAME_DEBUGGER" $DEBUGGER_ARGS ./$LAUNCHER_NAME -game hl -basedir "$GAMEROOT" $@
			;;
		*perf)
			"$GAME_DEBUGGER" record $DEBUGGER_ARGS ./$LAUNCHER_NAME -game hl -basedir "$GAMEROOT" $@
			;;
		*vtune*)
			# Source the vtune environment
			source vtune-env.sh
			"$VTUNE_COMMAND" -collect $VTUNE_TYPE $VTUNE_EXTRA_OPTS ./$LAUNCHER_NAME -game hl -basedir "$GAMEROOT" $@
			;;
	esac
else 
	./$LAUNCHER_NAME -basedir "$GAMEROOT" -game hl $@
fi
